package client.data.repository;

import java.util.List;

import io.reactivex.Observable;
import client.data.net.RestApi;
import client.domain.entity.LinkEntity;
import client.domain.repository.LinkRepository;

/**
 * Created by yurii on 6/26/17.
 */

public class TopLinkRepository extends RestApiRepository implements LinkRepository {

    public static final int ITEMS_PER_PAGE = 10;
    private final RestApi restApi;

    public TopLinkRepository(RestApi restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<List<LinkEntity>> getTopLinks(Integer count, String after) {
        return this.restApi.getTopLinks(count, ITEMS_PER_PAGE, after)
                .flatMap(linkWrapperResponse -> linkWrapperResponse.isSuccessful()
                        ? Observable.just(linkWrapperResponse.body().getData().getLinks())
                        : Observable.error(processErrorResponse(linkWrapperResponse)));
    }
}

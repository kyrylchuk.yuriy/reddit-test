package client.data.net.wrapper;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import client.domain.entity.LinkEntity;

/**
 * Created by yurii on 6/27/17.
 */

public class LinkEntityDeserializer implements JsonDeserializer<LinkEntity> {

    public static final String DATA_KEY = "data";
    public static final String TITLE_KEY = "title";
    public static final String AUTHOR_KEY = "author";
    public static final String THUMBNAIL_KEY = "thumbnail";
    public static final String CREATED_UTC_KEY = "created_utc";
    public static final String NUM_COMMENTS_KEY = "num_comments";
    public static final String NAME_KEY = "name";
    public static final String PREVIEW_KEY = "preview";
    public static final String IMAGES_KEY = "images";
    public static final String SOURCE_KEY = "source";
    public static final String URL_KEY = "url";

    @Override
    public LinkEntity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (!json.isJsonNull()) {
            JsonObject root = json.getAsJsonObject();
            if (root.has(DATA_KEY)) {
                JsonObject data = root.getAsJsonObject(DATA_KEY);
                LinkEntity linkEntity = new LinkEntity();
                if (data.has(NAME_KEY)) {
                    linkEntity.setName(data.get(NAME_KEY).getAsString());
                }
                if (data.has(TITLE_KEY)) {
                    linkEntity.setTitle(data.get(TITLE_KEY).getAsString());
                }
                if (data.has(AUTHOR_KEY)) {
                    linkEntity.setAuthor(data.get(AUTHOR_KEY).getAsString());
                }
                if (data.has(THUMBNAIL_KEY)) {
                    linkEntity.setThumbnail(data.get(THUMBNAIL_KEY).getAsString());
                }
                if (data.has(CREATED_UTC_KEY) && !data.get(CREATED_UTC_KEY).isJsonNull()) {
                    linkEntity.setCreatedUTC(data.get(CREATED_UTC_KEY).getAsLong() * 1000);
                }
                if (data.has(NUM_COMMENTS_KEY) && !data.get(NUM_COMMENTS_KEY).isJsonNull()) {
                    linkEntity.setNumComments(data.get(NUM_COMMENTS_KEY).getAsInt());
                }
                if (data.has(PREVIEW_KEY) && data.get(PREVIEW_KEY).getAsJsonObject().has(IMAGES_KEY)) {
                    JsonObject preview = data.get(PREVIEW_KEY).getAsJsonObject();
                    if (preview.has(IMAGES_KEY) && preview.get(IMAGES_KEY).getAsJsonArray().size() > 0) {
                        JsonArray images = preview.get(IMAGES_KEY).getAsJsonArray();
                        if (images.get(0).getAsJsonObject().has(SOURCE_KEY)) {
                            JsonObject source = images.get(0).getAsJsonObject().get(SOURCE_KEY).getAsJsonObject();
                            if (source.has(URL_KEY)) {
                                linkEntity.setOriginalImage(source.get(URL_KEY).getAsString());
                            }
                        }
                    }
                }
                return linkEntity;
            }
        }
        return null;
    }
}

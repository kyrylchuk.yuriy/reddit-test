package client.data.net.wrapper;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import client.domain.entity.LinkEntity;

/**
 * Created by yurii on 6/27/17.
 */

public class TopLinksWrapper {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @SerializedName("children")
        private List<LinkEntity> links;

        public List<LinkEntity> getLinks() {
            return links;
        }

        public void setLinks(List<LinkEntity> links) {
            this.links = links;
        }
    }
}

package client.data.net;

import io.reactivex.Observable;
import client.data.net.wrapper.TopLinksWrapper;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestApi {

    String URL_BASE = "https://www.reddit.com/";

    @GET("/top.json")
    Observable<Response<TopLinksWrapper>> getTopLinks(@Query("count") Integer count,
                                                      @Query("limit") int limit,
                                                      @Query("after") String afterLink);
}

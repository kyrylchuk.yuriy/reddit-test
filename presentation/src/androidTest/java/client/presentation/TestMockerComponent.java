package client.presentation;

import client.presentation.main.MainScope;
import client.presentation.dependency.component.ApplicationComponent;

import dagger.Component;

@MainScope
@Component(modules = TestMockerModule.class, dependencies = ApplicationComponent.class)
public interface TestMockerComponent {}

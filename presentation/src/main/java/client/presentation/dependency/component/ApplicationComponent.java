package client.presentation.dependency.component;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import io.reactivex.Scheduler;
import client.data.net.RestApi;
import client.domain.repository.LinkRepository;
import client.presentation.dependency.module.ApplicationModule;
import client.presentation.dependency.module.DataModule;

@Singleton
@Component(modules = { ApplicationModule.class, DataModule.class })
public interface ApplicationComponent {

    Context context();
    SharedPreferences sharedPreferences();

    @Named("UI")
    Scheduler uiScheduler();
    @Named("IO")
    Scheduler ioScheduler();

    RestApi restApi();
    LinkRepository linkRepository();
}

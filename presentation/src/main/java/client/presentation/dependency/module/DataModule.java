package client.presentation.dependency.module;

import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import client.data.net.RestApi;
import client.data.net.interceptor.ConnectivityInterceptor;
import client.data.net.interceptor.HttpInterceptor;
import client.data.net.wrapper.LinkEntityDeserializer;
import client.data.repository.TopLinkRepository;
import client.domain.entity.LinkEntity;
import client.domain.repository.LinkRepository;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class DataModule {

    @Provides
    @Singleton
    RestApi provideRestApi(Context context) {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .addInterceptor(new ConnectivityInterceptor(context))
                .addInterceptor(new HttpInterceptor())
                .build();

        GsonConverterFactory factory = GsonConverterFactory.create(new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(LinkEntity.class, new LinkEntityDeserializer())
                .create());

        return new Retrofit.Builder()
                .baseUrl(RestApi.URL_BASE)
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build()
                .create(RestApi.class);
    }

    @Provides
    @Singleton
    LinkRepository provideLinkRepository(RestApi restApi) {
        return new TopLinkRepository(restApi);
    }

}

package client.presentation.main;

import dagger.Component;
import client.presentation.dependency.component.ApplicationComponent;

@MainScope
@Component(dependencies = ApplicationComponent.class, modules = MainModule.class)
public interface MainComponent {
    void inject(MainActivity mainActivity);
}

package client.presentation.main;

import dagger.Module;
import dagger.Provides;
import client.domain.interactor.link.GetTopLinksUseCase;
import client.presentation.Contracts.Main;

@Module
public class MainModule {

    @MainScope
    @Provides
    Main.PresenterMVP provideMainPresenter(GetTopLinksUseCase getTopLinksUseCase) {
        return new MainPresenter(getTopLinksUseCase);
    }
}

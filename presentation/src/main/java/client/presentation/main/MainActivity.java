package client.presentation.main;

import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import client.domain.entity.LinkEntity;
import client.presentation.Contracts.Main;
import client.presentation.R;
import client.presentation.main.adapter.LinkAdapter;
import client.presentation.main.adapter.OnThumbnailClickListener;
import client.presentation.view.activity.base.BaseActivity;
import client.presentation.view.adapter.EndlessRecyclerViewScrollListener;

public class MainActivity extends BaseActivity<Main.ViewMVP, Main.PresenterMVP, MainViewState>
        implements Main.ViewMVP, OnThumbnailClickListener {

    public static final int TOP_LINKS_LIMIT = 50;

    @Inject
    protected Main.PresenterMVP mainPresenter;

    @Bind(R.id.toolbar)
    protected Toolbar toolbar;
    @Bind(R.id.swipeRefreshLayout)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.linksRecyclerView)
    protected RecyclerView linksRecyclerView;
    private LinkAdapter linkAdapter = new LinkAdapter(Collections.emptyList(), this);
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private boolean reset;
    private AlertDialog thumbnailInteractionDialog;
    private DownloadManager downloadManager;

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void callInjector() {
        DaggerMainComponent.builder()
                .applicationComponent(getAppComponent())
                .mainModule(new MainModule())
                .build()
                .inject(this);
    }

    @NonNull
    @Override
    public Main.PresenterMVP createPresenter() {
        return mainPresenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linksRecyclerView.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (totalItemsCount != TOP_LINKS_LIMIT) {
                    presenter.getTopLinksPage(totalItemsCount);
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    viewState.setEndlessScrollListenerState(this.saveState());
                }
            }
        };
        swipeRefreshLayout.setOnRefreshListener(this::initialDataFetch);
        linksRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        linksRecyclerView.setAdapter(linkAdapter);
        thumbnailInteractionDialog = new AlertDialog.Builder(this)
                .setTitle(R.string.image_interaction_title)
                .setMessage(R.string.image_interaction_message)
                .setOnDismissListener(dialog -> viewState.setSelectedImagePosition(-1))
                .create();
        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
    }

    private void initialDataFetch() {
        reset = true;
        presenter.getTopLinksFirstPage();
    }

    @Override
    public void showMessage(String message) {
        toast(message);
    }

    @Override
    public void displayLogin() {
        //ignore now
    }

    @Override
    public void showTopLinksPage(List<LinkEntity> links, String pageId) {
        if (reset || pageId == null) {
            linkAdapter.changeData(links);
            endlessRecyclerViewScrollListener.reset();
            reset = false;
        } else {
            linkAdapter.addItems(links);
        }
        viewState.setLinks(linkAdapter.getItems());
    }

    @Override
    public void restoreEndlessScrollState(Bundle bundle) {
        if (bundle != null) {
            endlessRecyclerViewScrollListener.restoreState(bundle);
        }
    }

    @NonNull
    @Override
    public MainViewState createViewState() {
        return new MainViewState();
    }

    @Override
    public void onNewViewStateInstance() {
        initialDataFetch();
    }

    @Override
    public void onThumbnailClicked(int position) {
        presenter.selectThumbnail(position);
    }

    @Override
    public void showThumbnailInteractionDialog(int position) {
        String fullSizedImage = linkAdapter.getItem(position).getOriginalImage();
        if (!TextUtils.isEmpty(fullSizedImage)) {
            thumbnailInteractionDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Save", (dialog, which) -> {
                enqueueImageDownload(Uri.parse(fullSizedImage));
                toast(R.string.download_message);
            });
            thumbnailInteractionDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Open in web", (dialog, which) -> {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(fullSizedImage));
                startActivity(intent);
            });
            thumbnailInteractionDialog.show();
            viewState.setSelectedImagePosition(position);
        }
    }

    private void enqueueImageDownload(Uri imageUri) {
        String fileName = imageUri.getLastPathSegment();
        String imageType = fileName.substring(fileName.lastIndexOf('.') + 1);
        DownloadManager.Request request = new DownloadManager.Request(imageUri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false)
                .setTitle(fileName)
                .setMimeType("image/" + imageType)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                .allowScanningByMediaScanner();

        downloadManager.enqueue(request);
    }

    @Override
    public void showLoader() {
        swipeRefreshLayout.setRefreshing(true);
        viewState.setLoading(true);
    }

    @Override
    public void hideLoader() {
        swipeRefreshLayout.setRefreshing(false);
        viewState.setLoading(false);
    }
}

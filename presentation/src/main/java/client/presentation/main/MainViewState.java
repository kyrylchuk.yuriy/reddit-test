package client.presentation.main;

import android.os.Bundle;

import com.hannesdorfmann.mosby3.mvp.viewstate.ViewState;

import java.util.List;

import client.domain.entity.LinkEntity;
import client.presentation.Contracts;

/**
 * Created by yurii on 6/28/17.
 */

public class MainViewState implements ViewState<Contracts.Main.ViewMVP> {

    private Bundle endlessScrollListenerState;
    private List<LinkEntity> links;
    private boolean isLoading;
    private int selectedImagePosition = -1;
    private int requestedPermissionImagePosition = -1;

    @Override
    public void apply(Contracts.Main.ViewMVP view, boolean retained) {
        if (isLoading) {
            view.showLoader();
        } else {
            view.hideLoader();
        }
        if (links != null && !links.isEmpty()) {
            view.showTopLinksPage(links, links.get(links.size() - 1).getName());
        }
        view.restoreEndlessScrollState(endlessScrollListenerState);
        if (selectedImagePosition != -1 || requestedPermissionImagePosition != -1) {
            view.showThumbnailInteractionDialog(selectedImagePosition);
        }
    }

    public void setEndlessScrollListenerState(Bundle endlessScrollListenerState) {
        this.endlessScrollListenerState = endlessScrollListenerState;
    }

    public void setLinks(List<LinkEntity> links) {
        this.links = links;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public void setSelectedImagePosition(int selectedImagePosition) {
        this.selectedImagePosition = selectedImagePosition;
    }

    public void setRequestedPermissionImagePosition(int requestedPermissionImagePosition) {
        this.requestedPermissionImagePosition = requestedPermissionImagePosition;
    }
}

package client.presentation.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Date;

import butterknife.Bind;
import client.domain.entity.LinkEntity;
import client.presentation.R;
import client.presentation.view.adapter.BaseViewHolder;

/**
 * Created by yurii on 6/28/17.
 */

public class LinkViewHolder extends BaseViewHolder<LinkEntity> {

    @Bind(R.id.thumbnailImageView)
    protected ImageView thumbnailImageView;
    @Bind(R.id.authorTextView)
    protected TextView authorTextView;
    @Bind(R.id.titleTextView)
    protected TextView titleTextView;
    @Bind(R.id.createdAgoTextView)
    protected TextView createdAgoTextView;
    @Bind(R.id.commentsCountTextView)
    protected TextView commentsCountTextView;
    @Bind(R.id.linkNumber)
    protected TextView linkNumber;

    public LinkViewHolder(View itemView, OnThumbnailClickListener onThumbnailClickListener) {
        super(itemView);
        if (onThumbnailClickListener != null) {
            thumbnailImageView.setOnClickListener(v -> onThumbnailClickListener.onThumbnailClicked(getAdapterPosition()));
        }
    }

    @Override
    public void bind(@NonNull LinkEntity linkEntity) {
        Context context = itemView.getContext();
        if (!TextUtils.isEmpty(linkEntity.getThumbnail())) {
            Glide.with(context)
                    .load(linkEntity.getThumbnail())
                    .placeholder(R.drawable.ic_reddit_placeholder)
                    .into(thumbnailImageView);
        } else {
            thumbnailImageView.setImageResource(R.drawable.ic_reddit_placeholder);
        }
        authorTextView.setText(context.getString(R.string.author_formatting, linkEntity.getAuthor()));
        titleTextView.setText(linkEntity.getTitle());
        createdAgoTextView.setText(DateUtils.getRelativeTimeSpanString(linkEntity.getCreatedUTC(), new Date().getTime(), DateUtils.SECOND_IN_MILLIS));
        commentsCountTextView.setText(String.valueOf(linkEntity.getNumComments()));
        linkNumber.setText("#" + (getAdapterPosition() + 1));
    }
}

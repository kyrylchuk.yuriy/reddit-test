package client.presentation.main.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import client.domain.entity.LinkEntity;
import client.presentation.R;
import client.presentation.view.adapter.BaseRecyclerViewAdapter;
import client.presentation.view.adapter.BaseViewHolder;

/**
 * Created by yurii on 6/28/17.
 */

public class LinkAdapter extends BaseRecyclerViewAdapter<LinkEntity> {

    private OnThumbnailClickListener onThumbnailClickListener;

    public LinkAdapter(List<LinkEntity> data, OnThumbnailClickListener onThumbnailClickListener) {
        super(data);
        this.onThumbnailClickListener = onThumbnailClickListener;
    }

    @Override
    public BaseViewHolder<LinkEntity> onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.link_item, parent, false);
        return new LinkViewHolder(view, onThumbnailClickListener);
    }
}

package client.presentation.main.adapter;

/**
 * Created by yurii on 6/28/17.
 */

public interface OnThumbnailClickListener {
    void onThumbnailClicked(int position);
}

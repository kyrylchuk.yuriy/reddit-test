package client.presentation.main;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;
import client.domain.entity.LinkEntity;
import client.domain.interactor.link.GetTopLinksUseCase;
import client.presentation.Contracts.Main;
import client.presentation.presenter.BasePresenter;

/**
 * Created on 27.04.17.
 */

public class MainPresenter extends BasePresenter<Main.ViewMVP>
        implements Main.PresenterMVP {

    private GetTopLinksUseCase getTopLinksUseCase;
    private TopLinksSubscriber topLinksSubscriber;
    private String nextPageId;

    @Inject
    public MainPresenter(GetTopLinksUseCase getTopLinksUseCase) {
        this.getTopLinksUseCase = getTopLinksUseCase;
    }

    @Override
    public void getTopLinksFirstPage() {
        nextPageId = null;
        getTopLinksPage(null);
    }

    @Override
    public void getTopLinksPage(Integer count) {
        getTopLinksUseCase.setParams(count, nextPageId);
        topLinksSubscriber = new TopLinksSubscriber();
        if (hasView()) {
            view.showLoader();
        }
        getTopLinksUseCase.execute(topLinksSubscriber);
    }

    @Override
    public void selectThumbnail(int position) {
        if (hasView()) {
            view.showThumbnailInteractionDialog(position);
        }
    }

    @Override
    public void detachView(boolean retainInstance) {
        if (topLinksSubscriber != null && !topLinksSubscriber.isDisposed()) {
            topLinksSubscriber.dispose();
        }
        super.detachView(retainInstance);
    }

    class TopLinksSubscriber extends DisposableObserver<List<LinkEntity>> {

        @Override
        public void onNext(List<LinkEntity> links) {
            if (hasView()) {
                view.showTopLinksPage(links, nextPageId);
                if (links != null && !links.isEmpty()) {
                    nextPageId = links.get(links.size() - 1).getName();
                }
            }
        }

        @Override
        public void onError(Throwable e) {
            if (hasView()) {
                view.showMessage(e.getMessage());
                view.hideLoader();
            }
        }

        @Override
        public void onComplete() {
            if (hasView()) {
                view.hideLoader();
            }
        }

    }

}

package client.presentation.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by Yurii Kyrylchuk on 31.01.17.
 */

public abstract class BaseViewHolder<Model> extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    /**
     * Used to bind concrete model to the view holder
     *
     * @param model data source
     */
    public abstract void bind(@NonNull Model model);
}

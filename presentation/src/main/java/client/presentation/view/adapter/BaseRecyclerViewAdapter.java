package client.presentation.view.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yurii Kyrylchuk on 31.01.17.
 */

public abstract class BaseRecyclerViewAdapter<Model> extends RecyclerView.Adapter<BaseViewHolder<Model>> {

    protected List<Model> data;

    public BaseRecyclerViewAdapter(List<Model> data) {
        this.data = new ArrayList<>(data);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder<Model> holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void changeData(List<Model> data) {
        this.data = new ArrayList<>(data);
        notifyDataSetChanged();
    }

    @Nullable
    public Model getItem(int position) {
        if (position >= 0 && position < data.size()) {
            return data.get(position);
        }
        return null;
    }

    public List<Model> getItems() {
        return data;
    }

    public void addItem(Model item) {
        if (data.add(item)) {
            notifyItemInserted(data.size() - 1);
        }
    }

    public void addItem(Model item, int position) {
        if (position >= 0 && position < data.size()) {
            data.add(position, item);
            notifyItemInserted(position);
        }
    }

    public void addItems(List<Model> items) {
        if (items != null && !items.isEmpty()) {
            int previousSize = data.size();
            if (data.addAll(items)) {
                notifyItemRangeInserted(previousSize, items.size());
            }
        }
    }

    public void addItems(List<Model> items, int position) {
        if (position >= 0 && position < data.size() && items != null && !items.isEmpty()) {
            if (data.addAll(position, items)) {
                notifyItemRangeInserted(position, data.size());
            }
        }
    }

    /**
     * Remove provided item from the adapter
     * WARNING!!! This method may be slow, calc difficulty - O(n)
     *
     * @param item Item to remove
     */
    public void removeItem(Model item) {
        int position = data.indexOf(item);
        removeItem(position);
    }

    public void removeItem(int position) {
        if (position >= 0 && position < data.size()) {
            data.remove(position);
            notifyItemRemoved(position);
        }
    }

    public boolean isEmpty() {
        return data.isEmpty();
    }
}

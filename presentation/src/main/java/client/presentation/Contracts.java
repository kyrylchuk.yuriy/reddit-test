package client.presentation;

import android.os.Bundle;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import client.domain.entity.LinkEntity;

/**
 * Created on 27.04.17.
 */

public interface Contracts {

    interface BaseMvpView extends MvpView {
        void showLoader();
        void hideLoader();
        void handleError(Throwable error);
        void showMessage(String message);

        void displayLogin();
    }

    interface Main {
        interface ViewMVP extends BaseMvpView {
//           void showImage(String info);
            void showTopLinksPage(List<LinkEntity> links, String pageId);
            void restoreEndlessScrollState(Bundle bundle);
            void showThumbnailInteractionDialog(int position);
        }

        interface PresenterMVP extends MvpPresenter<ViewMVP> {
//            void getInfo(UserEntity user);
            void getTopLinksFirstPage();
            void getTopLinksPage(Integer count);
            void selectThumbnail(int position);
        }
    }

    // TODO Your view/presenter contracts goes here
}

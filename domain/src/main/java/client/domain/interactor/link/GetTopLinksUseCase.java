package client.domain.interactor.link;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import client.domain.entity.LinkEntity;
import client.domain.interactor.UseCase;
import client.domain.repository.LinkRepository;

/**
 * Created by yurii on 6/26/17.
 */

public class GetTopLinksUseCase extends UseCase<List<LinkEntity>> {

    private LinkRepository linkRepository;
    private Integer count;
    private String after;

    @Inject
    protected GetTopLinksUseCase(@Named("IO") Scheduler executionScheduler,
                                 @Named("UI") Scheduler postExecutionScheduler,
                                 LinkRepository linkRepository) {
        super(executionScheduler, postExecutionScheduler);
        this.linkRepository = linkRepository;
    }

    public void setParams(Integer count, String after) {
        this.count = count;
        this.after = after;
    }

    @Override
    protected Observable<List<LinkEntity>> buildUseCaseObservable() {
        return this.linkRepository.getTopLinks(count, after);
    }
}

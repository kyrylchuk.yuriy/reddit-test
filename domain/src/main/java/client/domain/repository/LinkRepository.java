package client.domain.repository;

import java.util.List;

import io.reactivex.Observable;
import client.domain.entity.LinkEntity;

/**
 * Created by yurii on 6/26/17.
 */

public interface LinkRepository {
    Observable<List<LinkEntity>> getTopLinks(Integer count, String after);
}

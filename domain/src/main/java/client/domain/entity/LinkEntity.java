package client.domain.entity;

/**
 * Created by yurii on 6/26/17.
 *
 * !!!IMPORTANT!!!
 *
 * If you want to change this model, then
 * you should modify @see client.data.net.wrapper.LinkEntityDeserializer and provide custom json parsing
 */

public class LinkEntity {
    private String title;
    private String author;
    private long createdUTC;
    private String thumbnail;
    private String originalImage;
    private int numComments;
    private String name;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getCreatedUTC() {
        return createdUTC;
    }

    public void setCreatedUTC(long createdUTC) {
        this.createdUTC = createdUTC;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getNumComments() {
        return numComments;
    }

    public void setNumComments(int numComments) {
        this.numComments = numComments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginalImage() {
        return originalImage;
    }

    public void setOriginalImage(String originalImage) {
        this.originalImage = originalImage;
    }
}

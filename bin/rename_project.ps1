Param(
  [string]$project,
  [string]$package
)

Write-Host "Replacing project name with $project"

gci ".\data", ".\presentation", ".\domain" -File -r |
 ForEach {(Get-Content $_.FullName | ForEach { $_ -replace '-project', $project }) | Set-Content $_.FullName} 
 
gci ".\" -File |
 ForEach {(Get-Content $_.FullName | ForEach { $_ -replace '-project', $project }) | Set-Content $_.FullName} 
 
Write-Host "Replacing package with $project"
 
gci ".\data", ".\presentation", ".\domain" -File -r |
 ForEach {(Get-Content $_.FullName | ForEach { $_ -replace 'RedditTest', $package }) | Set-Content $_.FullName} 
 
gci ".\" -File |
 ForEach {(Get-Content $_.FullName | ForEach { $_ -replace 'RedditTest', $package}) | Set-Content $_.FullName} 
 
$folder = $package.replace(".", "\");

Write-Host "New path $folder" 
 
New-Item ..\abtemp -type directory

Copy-Item .\data ..\abtemp -recurse
Copy-Item .\domain ..\abtemp -recurse
Copy-Item .\presentation ..\abtemp -recurse

Remove-Item .\domain\src\main\java\ua -recurse
New-Item .\domain\src\main\java\$folder\ -type directory
Copy-Item ..\abtemp\domain\src\main\java\RedditTest\domain .\domain\src\main\java\$folder -recurse

Remove-Item .\domain\src\test\java\ua -recurse
New-Item .\domain\src\test\java\$folder\ -type directory
Copy-Item ..\abtemp\domain\src\test\java\RedditTest\domain .\domain\src\test\java\$folder -recurse

Remove-Item .\data\src\main\java\ua -recurse
New-Item .\data\src\main\java\$folder\ -type directory
Copy-Item ..\abtemp\data\src\main\java\RedditTest\data .\data\src\main\java\$folder -recurse

Remove-Item .\data\src\test\java\ua -recurse
New-Item .\data\src\test\java\$folder\ -type directory
Copy-Item ..\abtemp\data\src\test\java\RedditTest\data .\data\src\test\java\$folder -recurse

Remove-Item .\data\src\androidTest\java\ua -recurse
New-Item .\data\src\androidTest\java\$folder\ -type directory
Copy-Item ..\abtemp\data\src\androidTest\java\RedditTest\data .\data\src\androidTest\java\$folder -recurse

Remove-Item .\presentation\src\main\java\ua -recurse
New-Item .\presentation\src\main\java\$folder\ -type directory
Copy-Item ..\abtemp\presentation\src\main\java\RedditTest\presentation .\presentation\src\main\java\$folder -recurse

Remove-Item .\presentation\src\test\java\ua -recurse
New-Item .\presentation\src\test\java\$folder\ -type directory
Copy-Item ..\abtemp\presentation\src\test\java\RedditTest\presentation .\presentation\src\test\java\$folder -recurse

Remove-Item .\presentation\src\androidTest\java\ua -recurse
New-Item .\presentation\src\androidTest\java\$folder\ -type directory
Copy-Item ..\abtemp\presentation\src\androidTest\java\RedditTest\presentation .\presentation\src\androidTest\java\$folder -recurse

Remove-Item ..\abtemp -recurse
